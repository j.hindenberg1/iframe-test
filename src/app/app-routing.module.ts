import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {FrameComponent} from './frame/frame.component';
import {TimerComponent} from './timer/timer.component';


const routes: Routes = [
  {
    path: '',
    component: FrameComponent,
  },
  {
    path: 'timer',
    component: TimerComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
