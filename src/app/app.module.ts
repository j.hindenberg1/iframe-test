import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TimerComponent } from './timer/timer.component';
import { FrameComponent } from './frame/frame.component';
import { ReloadSubscriberDirective } from './reload-subscriber/reload-subscriber.directive';

@NgModule({
  declarations: [
    AppComponent,
    TimerComponent,
    FrameComponent,
    ReloadSubscriberDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
