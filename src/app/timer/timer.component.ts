import { Component, OnInit } from '@angular/core';
import {interval} from 'rxjs';
import {last, map, takeWhile, tap} from 'rxjs/operators';

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.scss']
})
export class TimerComponent implements OnInit {

  timer = 10;

  constructor() { }

  ngOnInit(): void {

    interval(1000).pipe(
      takeWhile(timerValue => timerValue < 10),
      map(timerValue => 10 - timerValue),
      tap(timerValue => this.timer = timerValue),
      last(),
    ).subscribe(() => {
      window.location.reload();
    });

  }

}
