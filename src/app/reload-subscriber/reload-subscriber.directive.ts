








import {Directive, ElementRef, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {ReplaySubject, Subscription} from 'rxjs';
import {skip} from 'rxjs/operators';

@Directive({
  selector: '[appReloadSubscriber]'
})
export class ReloadSubscriberDirective implements OnInit, OnDestroy {

  @Output() reload = new EventEmitter<void>();

  private reload$ = new ReplaySubject<void>(1);











  // @todo: Replace by using BaseComponent

  private subscription: Subscription = null;

  constructor(
    private elementRef: ElementRef<HTMLIFrameElement>,
  ) { }

  ngOnInit() {
    this.elementRef.nativeElement.onload = () => {
      this.reload$.next();
    };

    // @todo: Replace by using BaseComponent
    this.subscription = this.reload$.pipe(
      skip(1),
    ).subscribe(() => {
      this.reload.next();
    });
  }

  // @todo: Replace by using BaseComponent
  ngOnDestroy() {
    if (!!this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
